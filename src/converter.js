/**
 * Padding outputs 2 characters allways
 * @param {string} hex one or two characters
 * @returns {string} hex with two characters
 */
const pad = (hex) => {
    return (hex.length === 1 ? "0" + hex : hex);
}

module.exports = {
    /**
     * Converts RGB to Hex string
     * @param {number} red 0-255
     * @param {number} green 0-255
     * @param {number} blue 0-255
     * @returns {string} hex value
     */
    rgbToHex: (red,green,blue) => {
        const redHex = red.toString(16); // may return single char
        const greenHex = green.toString(16);
        const blueHex = blue.toString(16);
        const hex = "#" + pad(redHex) + pad(greenHex) + pad(blueHex);
        return hex;
    },

    hexToRgb: (hex) => {
        var r = parseInt(hex.substring(0,2), 16);
        var g = parseInt(hex.substring(2,4), 16);
        var b = parseInt(hex.substring(4,6), 16);
        return "rgb(" + r + ", " + g + ", " + b + ")";
    }

    // hexToRgb: (hex) => {
    //     const r = parseInt(hex.slice(1, 3), 16),
    //     g = parseInt(hex.slice(3, 5), 16),
    //         b = parseInt(hex.slice(5, 7), 16);
    //         return "rgb(" + r + ", " + g + ", " + b + ")";
    // }
}