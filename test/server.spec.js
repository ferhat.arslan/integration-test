// ./test/server.spec.js - Integration test
const expect = require("chai").expect;
const request = require("request");
const app = require("../src/server");
const PORT = 3000;

describe("Color Code Converter API", () => {
    let server = undefined; // works without defining
    before("Starting server", (done) => {
        server = app.listen(PORT, () => {
            done();
        });
    });
    describe("RGB to Hex conversion", () => {
        const baseurl = `http://localhost:${PORT}`;
        it("returns status 200", (done) => {
            const url = baseurl + "/rgb-to-hex?r=255&g=0&b=0";
            request(url, (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });
        it("returns the color in hex", (done) => {
            request(baseurl, (error, response, body) => {
                expect(body).to.equal("ffffff");
                done();
            });
        });
    });
    
    after("Stop server", (done) => {
        server.close();
        done();
    });
});